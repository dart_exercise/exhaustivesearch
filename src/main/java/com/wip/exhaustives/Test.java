/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.exhaustives;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author WIP
 */
public class Test {

    public static void main(String[] args) {
        ExhaustiveS exs = new ExhaustiveS();
        ArrayList<Integer> list1 = new ArrayList<>();

        ArrayList<Integer> list2 = new ArrayList<>();

        int num = 10;

        Collections.addAll(list1, 1, 99, 34, 2, 45, 10, 16, 20, 7);
        Collections.addAll(list2, 1, 34, 2, 45, 9, 43, 20, 99, 8);

        System.out.println(exs.exhaustiveSearch(num, list1));
        System.out.println(exs.exhaustiveSearch(num, list2));
    }

}
